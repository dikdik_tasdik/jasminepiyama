<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Piyama Anak - Sendal Tidur Boneka - Baju Muslim</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>" type="text/css" media="all" />
        <!--[if lte IE 6]>
                <style type="text/css" media="screen">
			.tabbed { height:420px; }
		</style>
	<![endif]-->

        <script src="<?php echo base_url('assets/js/jquery-1.4.1.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/jquery/js/jquery-1.9.1.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/jquery/js/jquery-ui-1.10.3.custom.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/jquery/js/jquery-ui-1.10.3.custom.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.jcarousel.pack.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.slide.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery-func.js'); ?>" type="text/javascript"></script>        
        <script src="<?php echo base_url('assets/js/config_facebook.js'); ?>" type="text/javascript"></script>

    </head>
    <body>       

        <!-- Main -->
        <div id="main">
            <div class="shell">

                <!-- Search, etc -->
                <div class="options">
                    <div class="search">
                        <form action="" method="post">
                            <span class="field"><input type="text" class="blink" value="" title="Cari Produk Kami..." /></span>
                            <input type="text" class="search-submit" value="GO" />
                        </form>
                    </div>
                    <span class="left">
                        <?php
                        if ($this->session->userdata('sess_logout')) {//                            
                        ?>
                            <img title="<?php echo $this->session->userdata('sess_name') ?>" style="width: 30px; height: 30px; margin-top: -6px;" src="https://graph.facebook.com/<?php echo $this->session->userdata('sess_username'); ?>/picture">
                            <?php } else {
                            ?>                            
                            <img id="facebook_login" class="left" src="<?php echo base_url('assets/images/fb_connect.png'); ?>">

                                <?php } ?>
                                </span>
                                <div class="right">
                                    <div id="loading" class="left" style="margin-top: 4px; margin-right: 10px; display: none;">
                                        <img src="<?php echo base_url('assets/images/ajax-loader.gif') ?>">
                                    </div>
                                    <div class="cart shopping-cart">
                                        <div class="cart-ico" title="Jumlah Barang <?php echo $this->cart->total_items(); ?>/pcs"></div>
                                    </div>
            <!--                        <span class="cart shopping-cart">
                                        <a href="#" class="cart-ico" >&nbsp;</a>
                                        <strong><?php //echo $this->cart->total();  ?></strong>
                                    </span>-->
                                    <span class="left more-links">
                                        <a href="#">Checkout</a>
                                        <!--a href="#">Details</a-->
                                    </span>
                                </div>
                                </div>
                                <!-- End Search, etc -->

                                <!-- Content -->
                                <div id="content">

                                    <!-- Tabs -->
                                    <div class="tabs">
                                        <ul>
                                            <li><a href="#" class="active"><span>Piyama Anak</span></a></li>
                                            <li><a href="#"><span>Sendal Tidur Boneka</span></a></li>
                                            <li><a href="#"><span>Baju Muslim Dewasa</span></a></li>
                                            <li><a href="#" class="red"><span>Cara Belanja</span></a></li>
                                        </ul>
                                    </div>
                                    <!-- Tabs -->

                                    <!-- Container -->
                                    <div id="container">

                                        <div class="tabbed">

                                            <!-- First Tab Content -->
                                            <div style="margin-bottom: 10px;">1.2.3.4 >></div>
                                            <div class="tab-content" style="display:block;">
                                                <div class="items">
                                                    <div class="cl">&nbsp;</div>
                                                    <ul>
                                                        <?php foreach ($list_produk->result() as $row) {
                                                        ?>
                                                            <li>
                                                                <div class="item">
                                                                    <div class="image">
                                                                        <img src="<?php echo base_url('assets/images/angry_birds2.jpg') ?>" alt="" style="width: 120px; height: 140px;"/>
                                                                        <a href="<?php echo site_url('cart/add_to_cart/' . $row->id); ?>"></a>
                                                                    </div>
                                                                    <table>
                                                                        <tr>
                                                                            <td>Ukuran</td>
                                                                            <td>: (<?php echo $row->ukuran; ?>)</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Harga</td>
                                                                            <td>: Rp. <?php echo number_format($row->harga); ?></td>
                                                                        </tr>
                                                                    </table>
                                                                    <br/>
                                                                    <button class="add-to-cart" type="button" style="width:100%;">Beli</button>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="cl">&nbsp;</div>
                                                </div>
                                            </div>
                                            <div style="margin-bottom: 10px;">1.2.3.4 >></div>
                                            <!-- End First Tab Content -->

                                            <!-- Second Tab Content -->
                                            <div class="tab-content">
                                                <div class="items">
                                                    <div class="cl">&nbsp;</div>
                                                    <ul>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="<?php echo base_url('assets/images/sandal31.jpg') ?>" alt="" style="width: 120px; height: 140px;"/></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="<?php echo base_url('assets/images/sandal31.jpg') ?>" alt="" style="width: 120px; height: 140px;"/></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="<?php echo base_url('assets/images/sandal31.jpg') ?>" alt="" style="width: 120px; height: 140px;"/></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="<?php echo base_url('assets/images/sandal31.jpg') ?>" alt="" style="width: 120px; height: 140px;"/></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                    </ul>
                                                    <div class="cl">&nbsp;</div>
                                                </div>
                                            </div>
                                            <!-- End Second Tab Content -->

                                            <!-- Third Tab Content -->
                                            <div class="tab-content">
                                                <div class="items">
                                                    <div class="cl">&nbsp;</div>
                                                    <ul>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image3.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image3.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image3.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image3.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image4.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image4.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>


                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image2.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>
                                                        <li>
                                                            <div class="image">
                                                                <a href="#"><img src="assets/css/images/image1.jpg" alt="" /></a>
                                                            </div>
                                                            <p>
							    		Kode Produk: <span>125515</span><br />
							    		Ukuran : <span>8/8.5/9.5/10/11</span><br />

                                                            </p>
                                                            <p class="price">Harga/Pcs: <strong>Rp. 16.500</strong></p>
                                                        </li>

                                                    </ul>
                                                    <div class="cl">&nbsp;</div>
                                                </div>
                                            </div>
                                            <!-- End Third Tab Content -->
                                            <div class="tab-content">
                                                <div class="items" style="padding: 10px;">
                                                    <div class="cl">&nbsp;</div>

                                                    <strong>Cara Belanja</strong>
                                                    <ol>
                                                        <li>Pilih piyama yang anda  sukai dan klik tombol beli</li>
                                                        <li>Jika anda sudah selesai memilih barang yang anda pesan, selannjutnya klik tombol checkout</li>
                                                        <li>Masukan data untuk pengiriman barang.</li>
                                                        <li>Pesanan akan kami proses dan dikonfirmasi lewat email. Jumlah dan stock barang</li>
                                                        <li>Pembayaran dilakukan secara transfer</li>
                                                        <li>Barang akan dikirim sesuai dengan alamat yang diberikan sebelumnya</li>
                                                        <li>Kami akan mengirimkan NO RESI JNE ke nomor telepon anda.</li>
                                                    </ol>
                                                    <br/>
                                                    <strong>Informasi pengiriman:</strong>
                                                    <ol>
                                                        <li>Kami menggunakan jasa pengiriman JNE Reg, expedisi, pos indonesia atau pick up di tempat</li>
                                                        <li>Untuk jasa pengiriman JNE Reg akan membutuhkan 2-3 hari kerja untuk barang sampai di tujuan jika alamat rumah lengkap dan ada orang yang menunggu barang di rumah.</li>
                                                        <li>Untuk pengiriman ke kantor maka hanya akan dilakukan pengiriman oleh pihak JNE pada hari kerja saja.</li>
                                                        <li>Pengiriman via expedisi atau pos indonesia membutuhkan 5-10 hari kerja tergantung pihak pengiriman.</li>
                                                    </ol>
                                                    <br/>
                                                    <strong>Informasi Bank</strong>

                                                    <ol>
                                                        <li>
	Bank BJB
	REKENING 	: 02020202020
	NAMA		: Dikdik Tasdik Laksana
                                                        </li>
                                                        <li>
	Bank BCA
	REKENING 	: 02020202020
	NAMA		: Dikdik Tasdik Laksana
                                                        </li>
                                                        <li>
	Bank BRI
	REKENING 	: 02020202020
	NAMA		: Dikdik Tasdik Laksana
                                                        </li>
                                                    </ol>

                                                </div>
                                            </div>

                                        </div>

                                        <!-- Brands
				<div class="brands">
					<h3>Brands</h3>
					<div class="logos">
						<a href="#"><img src="css/images/logo1.gif" alt="" /></a>
						<a href="#"><img src="css/images/logo2.gif" alt="" /></a>
						<a href="#"><img src="css/images/logo3.gif" alt="" /></a>
						<a href="#"><img src="css/images/logo4.gif" alt="" /></a>
						<a href="#"><img src="css/images/logo5.gif" alt="" /></a>
					</div>
				</div>
				<!-- End Brands -->

                                        <!-- Footer -->
                                        <div id="footer">
                                            <!--div class="left">
						<a href="#">Home</a>
                                                    <span>|</span>
						<a href="#">Cara Belanja</a>
                                                    <span>|</span>
						<a href="#">My Account</a>
                                                    <span>|</span>
						<a href="#">The Store</a>
                                                    <span>|</span>
						<a href="#">Contact</a>
					</div-->
                                            <div class="right">
                                                &copy; 2013 Jasmine Piyama.
                                            </div>
                                        </div>
                                        <!-- End Footer -->

                                    </div>
                                    <!-- End Container -->

                                </div>
                                <!-- End Content -->

                                </div>
                                </div>
                                <!-- End Main -->




                                </body>
                                </html>