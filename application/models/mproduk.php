<?php

class MProduk extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_record($id) {
        $query = $this->db->query('SELECT * FROM produk WHERE id=' . $id . '');
        return $query->row();
    }

    function update_access_token($email, $access_token) {
        $data = array(
            'access_token' => $access_token,
        );

        $this->db->where('email', $email);
        $this->db->update('users', $data);
    }

}

?>
