<?php

class MUser extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function exists_record($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function update_access_token($email, $access_token) {
        $data = array(
            'access_token' => $access_token,
        );

        $this->db->where('email', $email);
        $this->db->update('users', $data);
    }

}

?>
