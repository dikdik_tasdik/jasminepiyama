<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include the facebook.php from libraries directory
require_once APPPATH . 'libraries/facebook/facebook.php';

class Fb extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->config->load('config_facebook');
        $this->load->model('muser', 'USER');
    }

    public function index() {
        $this->load->view('facebook/index');
    }

    public function logout() {
        $signed_request_cookie = 'fbsr_' . $this->config->item('appID');
        setcookie($signed_request_cookie, '', time() - 3600, "/");
        $this->session->sess_destroy();  //session destroy
        redirect('/welcome', 'refresh');  //redirect to the home page
    }

    public function fblogin() {

        $facebook = new Facebook(array(
                    'appId' => $this->config->item('appID'),
                    'secret' => $this->config->item('appSecret'),
                ));
// We may or may not have this data based on whether the user is logged in.
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.


        $user = $facebook->getUser(); // Get the facebook user id
        $profile = NULL;
        $logout = NULL;

        if ($user) {
            try {
                $profile = $facebook->api('/me');  //Get the facebook user profile data
                $access_token = $facebook->getAccessToken();
                $params = array('next' => base_url('fb/logout/'), 'access_token' => $access_token);
                $logout = $facebook->getLogoutUrl($params);
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = NULL;
            }

            //insert into users
            if ($this->USER->exists_record($profile['email']) == false) {
                $data_callback = array(
                    'name' => $profile['name'],
                    'username' => $profile['username'],
                    'email' => $profile['email'],
                    'provider' => 'facebook',
                    'provider_id' => $user,
                    'access_token' => $access_token
                );
                $this->db->insert('users', $data_callback);
            } else {
                $this->USER->update_access_token($profile['email'], $access_token);
            }

            $data['sess_user_id'] = $user;
            $data['sess_name'] = $profile['name'];
            $data['sess_username'] = $profile['username'];
            $data['sess_logout'] = $logout;

            $this->session->set_userdata($data);
            redirect('/welcome');
        }
    }

}

/* End of file fb.php */
/* Location: ./application/controllers/fb.php */