<?php

class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mproduk', 'PRODUK');
    }

    function index() {
//        $this->cart->destroy();
        $this->load->view('cart');
    }

    function add_to_cart($id) {
        $rs = $this->PRODUK->get_record($id);
        $data = array(
            'id' => $rs->id,
            'qty' => 1,
            'price' => $rs->harga,
            'name' => $rs->nama,
            'options' => array('Ukuran' => $rs->ukuran, 'Warna' => $rs->warna)
        );
        $this->cart->insert($data);
        echo true;
    }

}

?>
