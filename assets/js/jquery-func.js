$( document ).ready( function(){
    $('.blink')
    .focus(function(){
        if( $(this).attr('value') == $(this).attr('title') ) {
            $(this).attr({
                'value': ''
            });
        }
    })
    .blur(function(){
        if( $(this).attr('value') == '' || typeof($(this).attr('value')) == 'undefined') {
            $(this).attr({
                'value': $(this).attr('title')
            })
        }
    });
		
    $('#slider-holder ul').jcarousel({
        scroll: 1,
        wrap: 'both',
        initCallback: _init_carousel,
        buttonNextHTML: null,
        buttonPrevHTML: null
    });
	
    $('.tabs a').slide({
        'slide_selector' : '.tab-content'
    });


    //shopping cart
    $('.add-to-cart').on('click', function () {
        var cart = $('.shopping-cart');
        var imgtodrag = $(this).parent('.item').find("img").eq(0);
        var link = $(this).parent('.item').find("a").attr('href');
        
        if (imgtodrag) {
            $('#loading').show();
            var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000, 'easeInOutExpo');

            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);
            
            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
        $.get(link, function(data){          
            if(data){
                $('#loading').hide();
            }
        })
    });



});
function _init_carousel(carousel) {
    $('#slider-nav .next').bind('click', function() {
        carousel.next();
        return false;
    });
	
    $('#slider-nav .prev').bind('click', function() {
        carousel.prev();
        return false;
    });
};

function add_to_cart(link){
    $.get(link, function(data){
        if(data){
            
        }else{
            
    }
    });
};